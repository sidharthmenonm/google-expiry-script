function expirycheck() {
  var notification_email = "sidharth@startupmission.in" //email to send notification to
  var email_subject = "Expory Notification"
  
  var sheet_name = "Sheet1" //name of the sheet
  
  var expiry_col = "2" //column with the expiry dates
  var status_col = "3" //column with status
  
  var buffer_days = 1 //number of days before expiry notification should be send
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet(); //get active spreadsheet.
  var sheet = spreadsheet.getSheetByName(sheet_name); 
  
  var range = sheet.getRange(2, 1, sheet.getLastRow(), sheet.getLastColumn()) //get data starting from row 2 to the end
  
  var today = moment().add(buffer_days, 'days');
  
  for(var i = 1; i < range.getLastRow(); i++){
   var expiry_date = range.getCell(i, expiry_col).getValue();
   var status_column = range.getCell(i, status_col)
   var status = status_column.getValue();
   
    if(expiry_date && status==""){ // check if expiry date is not empty and status column is empty
     
      if(moment(expiry_date).isBefore(today)){
        //send notification and change status
        
        GmailApp.sendEmail(notification_email, email_subject, "Expired")
        
        status_column.setValue("Expired");
        
      }
     
   }
  }
  
  
}
